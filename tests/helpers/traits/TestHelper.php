<?php

namespace Tests\Helpers\Traits;

use Carbon\Carbon;
use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Core\Helpers\Enums\RateType;
use SmartUber\Core\Helpers\Enums\RequisitionAndTenderStatus;
use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Department\User;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\ProviderRequisition;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Requisition\Tender;

trait TestHelper
{
	public function getRequisition()
	{
		$date = Carbon::now();
        $startDate = $date->copy();
        $endDate = $startDate->copy()->addDays(1);
        $requisition = factory(Requisition::class)->create([
            "subject" => 'Admin',
            "description" => 'Admin',
            "start_date" => $startDate,
            "end_date" => $endDate
        ]);
        return $requisition;
	}

    public function getTender(Requisition $requisition)
    {
        $tender = factory(Tender::class)->create([
            "subject" => $requisition->subject,
            "description" => $requisition->description,
            "start_date" => $requisition->start_date,
            "end_date" => $requisition->end_date,
            'markup_price' => rand(1, 1000),
            'markup_rate_type' => rand(1, 1000),
            'rate_type' => rand(1, 1000),
            'requisition_id' => $requisition->id,
            'provider_id' => function() {
                return factory(Provider::class)->create()->id;
            }
        ]);
        return $tender;
    }

    public function getProviderRequisition(Requisition $requisition, $number = 1)
    {
        $providers = factory(Provider::class, $number)->create();
        $result = collect();
        $providers->each(function ($provider) use ($requisition, $result) {
            $providerRequisition = factory(ProviderRequisition::class)->create([
                "requisition_id" => $requisition->id,
                "provider_id" => $provider->id
            ]);
            $result->push($providerRequisition);
        });
        return $result;
    }

    public function getApplicant(Tender $tender, Member $member = null, $number = 1)
    {
        if (!$member) {
            $member = factory(Member::class, $number)->create();
        }
        $result = collect();
        $member->each(function ($member) use ($tender, $result) {
            $applicant = factory(Applicant::class)->create([
                "tender_id" => $tender->id,
                "member_id" => $member->id,
            ]);
            $result->push($applicant);
        });
        return $result;
    }

    public function isAutoAccept()
    {
        return config('core_config.auto_accept') === true;
    }

    public function getDummyCompanyModel($number = 10)
    {
        $company = factory(Company::class)->create();
        $branches = factory(Branch::class, $number)->create([
            'company_id' => $company->id
        ]);
        $users = factory(User::class, $number)->create();

        $users->each(function ($user) use ($company) {
            $providers = factory(Provider::class)->create([
                'company_id' => $company->id,
                'user_id' => $user->id
            ])
            ->each(function ($provider) use ($user) {
                $members = factory(Member::class)->create([
                    'provider_id' => $provider->id,
                    'user_id' => $user->id
                ]);
            });
        });

        $this->assertInstanceOf(Company::class, $company);
        $this->assertCount($number, $users);
        $this->assertCount($number, $company->branches);
        $this->assertCount($number, $company->providers);
        $this->assertCount($number, $company->providers()->first()->members);
        return $company;   
    } 

    public function getDummyOpenedRequisition(Company $company = null, Branch $branch = null, $number = 1)
    {
        $date = Carbon::now();
        if (!$company) {
            $company = $this->getDummyCompanyModel();
            $branch = $company->branches()->first();
        }

        $requisitions = collect();
        for ($i = 0; $i < $number; $i++) {
            $requisition = $company->openRequest($branch, 'Company Requisition', 'This is my first requisition', $date, $date->copy()->addWeeks(1));
            $requisitions->push($requisition);
        }

        $totalOpenedRequisition = $company->requisitions()->where('status', RequisitionAndTenderStatus::OPEN)->get();
        $this->assertCount($number, $company->requisitions);
        $this->assertCount($number, $totalOpenedRequisition);
        return $totalOpenedRequisition->count() > 1 ? $totalOpenedRequisition : $totalOpenedRequisition->first();
    }

    public function getDummyPostedRequisition(
        Company $company = null,
        Branch $branch = null,
        Provider $provider = null,
        $number = 1
    ) 
    {
        if (!$company) {
            $company = $this->getDummyCompanyModel();
            $branch = $company->branches()->first();
            $provider = $company->providers()->first();
        }

        $requisitions = $this->getDummyOpenedRequisition($company, $branch, $number);
        $requisitions->each(function($requisition, $index) use ($company) {
            $company->postRequest($requisition, Carbon::now(), rand(1, 100), rand(1, 100), RateType::HOURLY);
            $requisition = $company->requisitions()->where('id', $requisition->id)->first();
            $providers = $company->providers()->get()->count();
            $this->assertCount($company->providers()->get()->count(), $requisition->providerRequisitions);
            $this->assertEquals(RequisitionAndTenderStatus::POST, $requisition->status);
            $this->assertEquals(RateType::HOURLY, $requisition->rate_type);
        });

        return $requisitions->count() > 1 ? $requisitions : $requisitions->first();
    }

    public function getDummyTender(Requisition $requisition = null)
    {
        if (!$requisition) {
            $requisition = $this->getDummyPostedRequisition();
        }

        $company = $requisition->company;
        $branch = $requisition->branch;
        $provider = $company->providers()->first();

        $tender = $provider->openTender($requisition, RateType::HOURLY, rand(1, 100), RateType::HOURLY);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Company::class, $company);
        $this->assertInstanceOf(Branch::class, $branch);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionAndTenderStatus::OPEN, $tender->status);
        $this->assertEquals(RateType::HOURLY, $tender->rate_type);
        return $tender;
    }

    public function getDummyTenderWithApplicant(Requisition $requisition = null)
    {        
        $tender = $this->getDummyTender($requisition);
        $provider = $tender->provider;
        $company = $provider->company;
        $requisition = $tender->requisition;
        $date = Carbon::now();
        $members = $tender->provider->members;

        $members->each(function ($member, $index) use ($tender, $provider, $company) {
            $applicant = $member->applyTender($tender, Carbon::now());
            switch ($index) {
                case 0:
                case 1:
                case 2:
                    $provider->acceptApplicant($applicant);
                    break;
                case 3:
                case 4:
                case 5:
                    $company->acceptApplicant($applicant);
                    break;
                case 6:
                case 7:
                    $company->rejectApplicant($applicant);
                    break;
            }
        });

        $requisition = $company->requisitions()->where('id', $requisition->id)->first();
        $tender = $requisition->tenders()->where('requisition_id', $requisition->id)->where('id', $tender->id)->first();
        $totalAcceptedByProviderApplication = 
            $tender->applicants()->where('status', ApplicantStatus::ACCEPTED_BY_PROVIDER)->get();
        $totalSubmittedApplication = 
            $tender->applicants()->where('status', ApplicantStatus::SUBMITTED)->get();
        $totalAcceptedByCompanyApplication = 
            $tender->applicants()->where('status', ApplicantStatus::ACCEPTED_BY_COMPANY)->get();
        $totalRejectedByCompanyApplication = 
            $tender->applicants()->where('status', ApplicantStatus::REJECTED_BY_COMPANY)->get();

        $this->assertCount(10, $tender->applicants);
        $this->assertCount(10, $provider->members);
        $this->assertCount(5, $totalAcceptedByProviderApplication);
        $this->assertCount(3, $totalAcceptedByCompanyApplication);
        $this->assertCount(2, $totalRejectedByCompanyApplication);
        $this->assertCount(0, $totalSubmittedApplication);
        return $tender;
    }
}
