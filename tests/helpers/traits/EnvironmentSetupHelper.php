<?php

namespace Tests\Helpers\Traits;

trait EnvironmentSetupHelper
{
    public function setUp()
    {
        parent::setUp();

        $this->artisan('migrate', ['--database' => 'smart-uber']);

        $this->loadMigrationsFrom(realpath(__DIR__.'/../../../migrations'));
        $this->loadMigrationsFrom(realpath(__DIR__.'/../../../vendor/smart-uber/core/migrations'));
    }

    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'smart-uber');
        $app['config']->set('database.connections.smart-uber', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function getPackageProviders($app)
    {
        return [
            \SmartUber\Core\Providers\CoreServiceProvider::class,
            \SmartUber\Payment\Providers\PaymentServiceProvider::class,
            \Orchestra\Database\ConsoleServiceProvider::class
        ];
    }
}