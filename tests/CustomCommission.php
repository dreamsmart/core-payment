<?php

namespace Tests;

use SmartUber\Payment\Helpers\Contracts\CommissionContract;

class CustomCommission implements CommissionContract
{
    public function getPayableCommission()
    {
    	return "Payable Commission";
    }
}
