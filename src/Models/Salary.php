<?php

namespace SmartUber\Payment\Models;

use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Requisition\Tender;
use SmartUber\Payment\Helpers\Contracts\SalaryContract;
use SmartUber\Payment\Models\Base\BaseModel;

class Salary extends BaseModel implements SalaryContract
{
    public $timestamps = true;
    protected $fillable = [
        "salary", "payable_type"
    ];
    protected $guarded = [
        "created_at",
        "updated_at",
        "member_id",
        "tender_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function member()
    {
    	return $this->belongsTo(Member::class, 'member_id');
    }

    public function tender()
    {
    	return $this->belongsTo(Tender::class, 'tender_id');
    }

    public function getPayableSalary()
    {
    	return "PayableSalary";
    }
}
