<?php

namespace SmartUber\Payment\Models\Base;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{	
 	public static function isClassCommissionExists()
    {
        $commission = config('payment_config.commission');
        if (! class_exists($commission)) {
            return false;
        }

        if (! in_array(
        	"SmartUber\Payment\Helpers\Contracts\CommissionContract",
        	class_implements($commission))
        ) {
            return false;
        } 
        return true;
    }
    
    public static function isClassSalaryExists()
    {
        $salary = config('payment_config.salary');
        if (! class_exists($salary)) {
            return false;
        }

     	if (! in_array(
        	"SmartUber\Payment\Helpers\Contracts\SalaryContract",
        	class_implements($salary))
        ) {
            return false;
        }
        return true;
    }
}
