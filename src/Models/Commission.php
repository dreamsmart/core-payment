<?php

namespace SmartUber\Payment\Models;

use SmartUber\Core\Models\Department\Provider;
use SmartUber\Payment\Helpers\Contracts\CommissionContract;
use SmartUber\Payment\Models\Base\BaseModel;

class Commission extends BaseModel implements CommissionContract
{
 	public $timestamps = true;
    protected $fillable = [
        "commission", "payable_type"
    ];
    protected $guarded = [
        "created_at",
        "updated_at",
        "provider_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function provider()
    {
    	return $this->belongsTo(Provider::class, 'provider_id');
    }

    public function getPayableCommission()
    {
    	return "Payable Commission";
    }
}
