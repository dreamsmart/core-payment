<?php

namespace SmartUber\Payment\Models;

use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Payment\Helpers\Contracts\PaymentContract;
use SmartUber\Payment\Helpers\Enums\PayableType;
use SmartUber\Payment\Models\Base\BaseModel;

abstract class Payment extends BaseModel implements PaymentContract
{

    public static function calculateCommission($commission)
    {
        return $commission->getPayableCommission(); 
    }
    
    public static function calculateSalary($salary)
    {
        return $salary->getPayableSalary();  
    }

    public static function calculatePayment($tender, $startDate, $endDate)
    {   
        if (!self::isClassCommissionExists()) {
            return false;
        }
        if (!self::isClassSalaryExists()) {
            return false;
        }

        $result = [];
        $requisition = $tender->requisition;
        $commission = config("payment_config.commission");
        $commission = new $commission;
        $commission->provider()->associate($tender->provider);
        $commission->payable_type = PayableType::MONTHLY;
        $commission->commission = floatVal($requisition->max_price) - floatVal($tender->markup_price);
        $commission->save();
        $result['commission'] = self::calculateCommission($commission);

        $salary = config("payment_config.salary");
        $members = $tender->applicants()->where('status', ApplicantStatus::ACCEPTED_BY_COMPANY)->get();

        foreach ($members as $key => $member) {
            $salary = new $salary();
            $salary->member()->associate($member);
            $salary->tender()->associate($tender);
            $salary->payable_type = PayableType::MONTHLY;
            $salary->salary = $tender->markup_price;
            $salary->save();
            $result['salary'][] = self::calculateSalary($salary);
        }
        return $result;
    }
}
