<?php

namespace SmartUber\Payment\Providers;

use Illuminate\Support\ServiceProvider;
    
class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/PaymentConfig.php' => config_path('payment_config.php'),
            __DIR__.'/../../migrations/' => database_path('migrations')
        ], "payment-config");
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/PaymentConfig.php', 'payment_config');
    }
}
