<?php

namespace SmartUber\Payment\Helpers\Enums;

use SmartUber\Payment\Helpers\Enums\BaseEnum;

final class PayableType extends BaseEnum
{
    const HOURLY = 0;
    const DAILY = 1;
    const MONTHLY = 2;

    public static function getList()
    {
        return [
            self::HOURLY,
            self::DAILY,
            self::MONTHLY
        ];
    }

    public static function getString($val)
    {
        switch ($val) {
            case 0:
                return "Hourly";
            case 1:
                return "Daily";
            case 2:
                return "Monthly";
        }
    }
}
