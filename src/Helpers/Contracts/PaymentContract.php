<?php

namespace SmartUber\Payment\Helpers\Contracts;

interface PaymentContract
{
	public static function calculateCommission($commission);
	public static function calculateSalary($salary);
	public static function calculatePayment($tender, $startDate, $endDate);
}
