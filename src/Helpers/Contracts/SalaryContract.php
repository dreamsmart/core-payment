<?php

namespace SmartUber\Payment\Helpers\Contracts;

interface SalaryContract
{
	public function getPayableSalary();
}
