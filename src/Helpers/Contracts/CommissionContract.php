<?php

namespace SmartUber\Payment\Helpers\Contracts;

interface CommissionContract
{
	public function getPayableCommission();
}
